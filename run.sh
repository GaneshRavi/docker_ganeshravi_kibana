#!/bin/bash

set -e
ES_HOST=${ES_HOST:-"elastic"}

sed -i 's/"+window.location.hostname+"/'"$ES_HOST"'/g' /tomcat/webapps/ROOT/kibana/config.js

cat /tomcat/webapps/ROOT/kibana/config.js

if [ ! -f /.tomcat_admin_created ]; then
/usr/local/bin/create_tomcat_user.sh
fi
exec ${CATALINA_HOME}/bin/catalina.sh run
