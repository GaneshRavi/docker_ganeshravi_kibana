Kibana on tomcat 8
===================

Dockerfile for Kibana deployed on Tomcat 8

To check if Docker is correctly installed and running:

        # docker version


To build:

        # docker build -rm -t <USERNAME>/kibana .


To run:
To use Kibana with Elasticsearch, start the Elasticsearch container first and then provide its IP address as an argument when starting Kibana container.
Assuming the Elasticsearch container was started with the name 'elastic', use the following command to run this Kibana container:

	# docker run --name kibana -i -t -p 8080:8080 -e ES_HOST=$(docker inspect --format {{.NetworkSettings.IPAddress}} elastic) ganeshravi/kibana
